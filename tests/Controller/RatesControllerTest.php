<?php

namespace App\Tests\Controller;

use App\Entity\Currency;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RatesControllerTest extends WebTestCase
{
    /**
     * @todo
     * 1. metoda pobierania kursu aktualnego dla jednej waluty
     * 2. metoda pobierania kursu średniego dla waluty
     * 3. lista wszystkich walut
     */

    public function testGetRatesByCurrencyCode()
    {
        $client = static::createClient();
        $client->request('GET', '/api/currentRate/usd');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $jsonContent = $client->getResponse()->getContent();
        $this->assertJson($jsonContent);

        $arrayContent = json_decode($jsonContent, true);
        $this->assertArrayHasKey('code', $arrayContent);
        $this->assertArrayHasKey('exchangeRate', $arrayContent);
    }

    public function testIfGetRatesByCurrencyFailsWithFakeCurrencyCode()
    {
        $client = static::createClient();
        $client->request('GET', '/api/currentRate/fakeCurrency');

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $jsonContent = $client->getResponse()->getContent();
        $this->assertJson($jsonContent);

        $arrayContent = json_decode($jsonContent, true);
        $this->assertEquals(false, $arrayContent['success']);
        $this->assertArrayHasKey('message', $arrayContent);

    }

    public function testGetAllRates()
    {
        $client = static::createClient();
        $client->request('GET', '/api/rates');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $jsonContent = $client->getResponse()->getContent();

        $this->assertJson($jsonContent);

        $arrayContent = json_decode($jsonContent);
        $currencies = $client->getContainer()->get('doctrine')->getRepository(Currency::class)->findAll();

        $this->assertCount(count($currencies), $arrayContent);
    }

    public function testGetAverageRateForCurrency()
    {
        $client = static::createClient();
        $client->request('GET', '/api/averageRate/usd');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $jsonContent = $client->getResponse()->getContent();
        $this->assertJson($jsonContent);

        $arrayContent = json_decode($jsonContent, true);
        $this->assertArrayHasKey('code', $arrayContent);
        $this->assertArrayHasKey('averageExchangeRate', $arrayContent);
    }

    public function testIfGetAverageRateByCurrencyFailsWithFakeCurrencyCode()
    {
        $client = static::createClient();
        $client->request('GET', '/api/averageRate/fakeCurrency');

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $jsonContent = $client->getResponse()->getContent();
        $this->assertJson($jsonContent);

        $arrayContent = json_decode($jsonContent, true);
        $this->assertEquals(false, $arrayContent['success']);
        $this->assertArrayHasKey('message', $arrayContent);
    }

}
