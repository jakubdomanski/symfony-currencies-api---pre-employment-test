<?php

namespace App\Tests\NBPApi;

use App\NBPApi\ExchangeRates;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class NBPExchangeRatesTestPhpTest extends KernelTestCase
{

    public function testThatApiReturnsArrayOfRates()
    {
        $ratesService = new ExchangeRates();
        $exchangeRates = $ratesService->getAll();

        //nbp returns 149 currencies at the moment
        $this->assertEquals(149, count($exchangeRates));
    }
}
