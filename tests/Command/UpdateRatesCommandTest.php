<?php

namespace App\Tests\Command;

use App\Command\NbpUpdateRatesCommand;
use App\Entity\Rate;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class UpdateRatesCommandTest extends KernelTestCase
{
    public function testIfExecutingThisCommandWillUpdateRatesInDatabase()
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $container = $kernel->getContainer();
        $ratesRepo = $container
            ->get('doctrine')
            ->getRepository(Rate::class);

        $countOfRatesBeforeUpdate = count($ratesRepo->findAll());

        $application->add(new NbpUpdateRatesCommand(null, $container));
        $command = $application->find('nbp:rates:update');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);

        $countOfRatesAfterUpdate = count($ratesRepo->findAll());

        //nbp returns 149 currencies, test if only one rate is persisted for currency
        $this->assertEquals($countOfRatesBeforeUpdate + 149, $countOfRatesAfterUpdate);
    }
}