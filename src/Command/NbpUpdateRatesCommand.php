<?php

namespace App\Command;

use App\Entity\Currency;
use App\Entity\Rate;
use App\NBPApi\ExchangeRates;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NbpUpdateRatesCommand extends Command
{
    protected static $defaultName = 'nbp:rates:update';

    private $container;

    private $em;

    public function __construct($name = null, ContainerInterface $container)
    {
        parent::__construct($name);
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getManager();
    }

    protected function configure()
    {
        $this
            ->setDescription('Update rates to current on npb.pl');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $em = $this->getEm();
        $em->getConnection()->beginTransaction();

        try {
            $ratesService = new ExchangeRates();
            $exchangeRates = $ratesService->getAll();
            foreach ($exchangeRates as $exchangeRate) {
                $currency = $this->findCurrencyByCodeOrCreate($exchangeRate['code']);
                $this->addRate($currency, $exchangeRate['rate']);
            }
            $em->getConnection()->commit();
            $io->success('Rates updated successfully!');
        } catch (GuzzleException $e) {
            //todo log it somewhere
        } catch (DBALException $e) {
            //if some error exist with database do rollback
            $em->getConnection()->rollBack();
            $io->error('Rates updated successfully!');
        }
    }

    /**
     * @param Currency $currency
     * @param float $rate
     *
     * @return Rate
     */
    private function addRate(Currency $currency, float $rate): Rate
    {
        $newRate = new Rate();
        $newRate
            ->setCurrency($currency)
            ->setRate($rate);
        $em = $this->getEm();
        $em->persist($newRate);
        $em->flush();

        return $newRate;
    }

    /**
     * @param string $code
     *
     * @return Currency
     */
    private function findCurrencyByCodeOrCreate(string $code): Currency
    {
        $em = $this->getEm();
        $currencyRepository = $em->getRepository(Currency::class);
        $currency = $currencyRepository->findOneBy(['code' => $code]);
        if (null === $currency) {
            $currency = new Currency();
            $currency->setCode($code);
            $em->persist($currency);
            $em->flush();
        }
        return $currency;
    }

    /**
     * @return EntityManagerInterface
     */
    private function getEm(): EntityManagerInterface
    {
        return $this->em;
    }
}
