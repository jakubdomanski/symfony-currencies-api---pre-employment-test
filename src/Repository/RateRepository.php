<?php

namespace App\Repository;

use App\Entity\Currency;
use App\Entity\Rate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Rate|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rate|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rate[]    findAll()
 * @method Rate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RateRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Rate::class);
    }

    /**
     * @param Currency $currency
     * @return int
     */
    public function getAverageByCurrency(Currency $currency)
    {
        return $this->createQueryBuilder('r')
            ->select("avg(r.rate)")
            ->andWhere('r.currency = :currency')
            ->setParameter('currency', $currency)
            ->getQuery()
            ->getResult();
    }
}
