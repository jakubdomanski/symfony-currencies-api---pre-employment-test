<?php

namespace App\NBPApi;

class ExchangeRates
{
    private $baseUrl = 'http://api.nbp.pl/api/exchangerates/tables/';

    private $rateTables = ['A', 'B'];

    private $defaultHeaders = ['Accept' => 'application/json'];

    private $rates = [];

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAll()
    {
        foreach ($this->rateTables as $rateTable) {
            $jsonResponse = $this->makeRequest($this->baseUrl . $rateTable . '?format=json', 'GET', $this->defaultHeaders);
            if ($jsonResponse->getStatusCode() === 200) {
                $this->extractRatesFromResponse($jsonResponse->getBody()->getContents());
            }
        }

        return $this->getRates();
    }


    /**
     * @param string $url
     * @param string $method
     * @param array $headers
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function makeRequest(string $url, string $method, array $headers)
    {
        $client = new \GuzzleHttp\Client($headers);
        $response = $client->request($method, $url);

        return $response;
    }

    /**
     * @return array
     */
    private function getRates(): array
    {
        return $this->rates;
    }

    private function addRate(string $currencyCode, float $rate): void
    {
        $date = new \DateTime();
        $arr = [
            'code' => $currencyCode,
            'rate' => $rate,
            'updateDate' => $date->getTimestamp()
        ];
        $this->rates[] = $arr;
    }

    private function extractRatesFromResponse($jsonResponse)
    {
        $arrayResponse = json_decode($jsonResponse, true);
        foreach ($arrayResponse[0]['rates'] as $currency) {
            $this->addRate($currency['code'], $currency['mid']);
        }
    }
}