<?php

namespace App\Controller;

use App\Entity\Currency;
use App\Entity\Rate;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class RateController extends AbstractController
{
    /**
     * @Route("/api/currentRate/{currencyCode}", name="currency_current_exchange_rate", methods={"GET"})
     *
     * @param string $currencyCode
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getLastRateForCurrency(string $currencyCode, EntityManagerInterface $em): JsonResponse
    {
        $currency = $em->getRepository(Currency::class)->findOneBy(['code' => strtoupper($currencyCode)]);
        if (null === $currency) {
            return $this->json([
                'success' => false,
                'message' => 'We have problem finding specified currency in our database. Check if that currency code exists!'
            ], 400);
        }
        $currentRate = $em->getRepository(Rate::class)->findOneBy(['currency' => $currency], ['createdAt' => 'desc']);
        return $this->json([
            'code' => $currency->getCode(),
            'exchangeRate' => $currentRate->getRate()
        ]);
    }

    /**
     * Get list of exchange rates along with currency code as json.
     *
     * @Route("/api/rates", name="get_current_rates", methods={"GET"})
     *
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getList(EntityManagerInterface $em): JsonResponse
    {
        $rates = [];
        $currencies = $em->getRepository(Currency::class)->findAll();
        foreach ($currencies as $currency) {
            $lastRateForCurrency = $em->getRepository(Rate::class)->findOneBy(['currency' => $currency], ['createdAt' => 'desc']);
            $tmpArray = [];
            $tmpArray['code'] = $currency->getCode();
            $tmpArray['rate'] = $lastRateForCurrency->getRate();
            $rates[] = $tmpArray;
        }
        return $this->json($rates);
    }

    /**
     * @Route("/api/averageRate/{currencyCode}", name="currency_average_exchange_rate", methods={"GET"})
     *
     * @param string $currencyCode
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getAverageRateForCurrency(string $currencyCode, EntityManagerInterface $em): JsonResponse
    {
        $currency = $em->getRepository(Currency::class)->findOneBy(['code' => strtoupper($currencyCode)]);
        if (null === $currency) {
            return $this->json([
                'success' => false,
                'message' => 'We have problem finding specified currency in our database. Check if that currency code exists!'
            ], 400);
        }
        $average = $em->getRepository(Rate::class)->getAverageByCurrency($currency);

        return $this->json([
            'code' => $currency->getCode(),
            'averageExchangeRate' => $average
        ]);
    }
}
